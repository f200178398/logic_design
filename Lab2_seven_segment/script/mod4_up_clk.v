// a mod 4 clk
module mod4_up_clk(
    input clk,reset_n,
    output reg [1:0] qout
);



//body of mod 4 clk
always @(posedge clk or negedge reset_n)
    begin
        if(!reset_n) 
            qout<=0;
        else
            qout<=qout+2'b01;

    end
endmodule