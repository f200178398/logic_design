// a decoder_2to4
module mux_4to1_4bits(
    input [1:0] sel,
    input [3:0] a_in, b_in, c_in, d_in,
    output reg [3:0] y_out
);


//body of mux_4to1_4bits
always @(*)
    begin
      case(sel)
        2'b00: y_out = a_in;
        2'b01: y_out = b_in;
        2'b10: y_out = c_in;
        2'b11: y_out = d_in;
      endcase

    end
endmodule