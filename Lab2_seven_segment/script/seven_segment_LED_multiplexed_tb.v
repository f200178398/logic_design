// a test bench for the multiplexed seven-segment
// LED display module
`timescale 1 ns / 100 ps
module seven_segment_LED_multiplexed_tb;
// internal signals declarations:
reg  clk_1ms, reset_n;
reg  [3:0] a_in, b_in, c_in, d_in;
wire [3:0] addr;
wire a, b, c, d, e, f, g;
// Unit Under Test instantiation and port map
seven_segment_LED_multiplexed UUT (
      .clk_1ms(clk_1ms), .reset_n(reset_n),
      .a_in(a_in), .b_in(b_in), 
      .c_in(c_in), .d_in(d_in), .addr(addr), .a(a), 
      .b(b), .c(c), .d(d), .e(e), .f(f), .g(g));
 
//integer i;
always begin // generate the clk_1ms signal
   #10 clk_1ms <= 1'b0;
   #10 clk_1ms <= 1'b1;
end
initial a_in=0;
initial b_in=1;
initial c_in=2;
initial d_in=3;
initial begin  
   reset_n <= 1'b0; 	// generate the reset_n signal
   #10 reset_n <= 1'b1;
   // generate input data

end
initial
   #7000 $finish;
initial       // response monitoring block
   $monitor($realtime,"ns %h %h %h %h %h %h %h %h %h %h %h %h %h %h \n",
                       clk_1ms, reset_n, a_in, b_in, c_in, d_in, addr, 
                       a, b, c, d, e, f, g);
endmodule