// a test bench for the decoder_2to4 module
`timescale 1 ns/100 ps
module decoder_2to4_tb;
// internal signals declaration

	reg [1:0] x_in;
	wire [3:0] y_out;

	//Unit Under Test instance and port map
	decoder_2to4 UUT(.x_in(x_in),.y_out(y_out));
    integer i;
        
	initial begin //stimulus generation block
        for(i=0;i<=31;i=i+1)begin
                 x_in=i[1:0];
					  #10;
        end	
	end 
   
	initial #250 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h",x_in,y_out);
endmodule
