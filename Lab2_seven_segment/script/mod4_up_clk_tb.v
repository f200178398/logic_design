// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module mod4_up_clk_tb;
// internal signals declaration

	reg clk,reset_n;
	wire [1:0] qout;

	//Unit Under Test instance and port map
	mod4_up_clk UUT(.clk(clk),.reset_n(reset_n),.qout(qout));

	always begin //stimulus generation block
		
		#10;
		clk<=1'b0;
        #10;
        clk<=1'b1;
		
	end 
    initial begin
        #10 reset_n<=1'b0;
        #10 reset_n<=1'b1;

    end
	initial #250 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h",clk,reset_n,qout);
endmodule
