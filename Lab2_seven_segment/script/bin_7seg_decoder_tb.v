// a test bench for bin_7seg_decoder
`timescale 1 ns/100 ps
module bin_7seg_decoder_tb;
// internal signals declaration

	wire b3,b2,b1,b0;
	wire g,f,e,d,c,b,a;
    wire [6:0] data_out;
    reg [3:0] bin;
	bin_7seg_decoder UUT(.b3(b3),.b2(b2),.b1(b1),.b0(b0),.g(g),.f(f),.e(e),.d(d),.c(c),.b(b),.a(a));
   
	integer i;
   assign    {b3,b2,b1,b0} = bin;
	assign    data_out = {g,f,e,d,c,b,a}; //assign 要用wire =後面都可以用
	initial begin //stimulus generation block
        for(i=0;i<=31;i = i+1)begin
		  
            bin = i[3:0];
				#20;
        end	
	end 
   
	initial #250 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h  %h %h  %h %h  %h %h  %h %h",bin,g,f,e,d,c,b,a,data_out);
endmodule
