// a decoder_2to4
module decoder_2to4(
    input [1:0] x_in,
    output reg [3:0] y_out 
);



//body of decoder_2to4
always @(x_in)
    begin
      case(x_in)
        2'b00:y_out=4'b1110;
        2'b01:y_out=4'b1101;
        2'b10:y_out=4'b1011;
        2'b11:y_out=4'b0111;
      endcase

    end
endmodule