// the top-level module of the four-digit multiplexed
// seven-segment LED display system
module seven_segment_LED_multiplexed ( 
       input  clk_1ms, reset_n,
       input  [3:0] a_in, b_in, c_in, d_in,
       output [3:0] addr,
       output a, b, c, d, e, f, g);
// the body of the module
wire [3:0] y_out;
wire [1:0] sel;
wire [6:0] data_out;
wire b3, b2, b1, b0;
// instantiate needed modules
assign {b3, b2, b1, b0} = y_out;
mod4_up_clk  mux_selector(clk_1ms, reset_n, sel);

//module decoder_2to4(input [1:0] x_in,output reg [3:0] y_out );
decoder_2to4 LED_sel (sel,addr);
//module mux_4to1_4bits(input [1:0] sel,input [3:0] a_in, b_in, c_in, d_in, output reg [3:0] y_out);
mux_4to1_4bits mux_binary(sel,a_in, b_in, c_in, d_in,y_out);
//module bin_7seg_decoder( input b3,b2,b1,b0,output g,f,e,d,c,b,a);
bin_7seg_decoder segment_seg_LED(b3, b2, b1, b0,g,f,e,d,c,b,a);
endmodule 