// a test bench for the decoder_2to4 module
`timescale 1 ns/100 ps
module mux_4to1_4bits_tb;
// internal signals declaration

reg [1:0] sel;
reg [3:0] a_in, b_in, c_in, d_in;
wire [3:0] y_out;

	//Unit Under Test instance and port map
	mux_4to1_4bits UUT (.sel(sel),.a_in(a_in),.b_in(b_in),
	                    .c_in(c_in),.d_in(d_in),.y_out(y_out));
    
integer i;
        
   initial a_in=0;
   initial b_in=1;
   initial c_in=2;
   initial d_in=3;
   initial begin //stimulus generation block
        for(i=0;i<=31;i=i+1)begin
                #20 sel=i[1:0];
        end	
	end 
   
	initial #250 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h %h %h", sel, a_in, b_in, c_in, d_in, y_out);
endmodule
