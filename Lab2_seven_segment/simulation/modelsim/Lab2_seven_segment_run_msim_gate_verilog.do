transcript on
if {[file exists gate_work]} {
	vdel -lib gate_work -all
}
vlib gate_work
vmap work gate_work

vlog -vlog01compat -work work +incdir+. {Lab2_seven_segment_7_1200mv_85c_slow.vo}

vlog -vlog01compat -work work +incdir+D:/DigitalDesignlabs/Lab2_seven_segment/script {D:/DigitalDesignlabs/Lab2_seven_segment/script/bin_7seg_decoder_tb.v}

vsim -t 1ps +transport_int_delays +transport_path_delays -L altera_ver -L cycloneive_ver -L gate_work -L work -voptargs="+acc"  bin_7seg_decoder_tb

add wave *
view structure
view signals
run -all
