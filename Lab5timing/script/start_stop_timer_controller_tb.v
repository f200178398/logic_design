// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module start_stop_timer_controller_tb;
// internal signals declaration

	 reg clear_button,start_button;
    wire clear,clk;
	 reg clk_10ms,reset;
    wire qout_T_FF;
	
	//Unit Under Test instance and port map
	start_stop_timer_controller UUT(.clear_button(clear_button),.start_button(start_button),.clk_10ms(clk_10ms),.reset(reset),.clear(clear),.clk(clk),.qout_T_FF(qout_T_FF));
    
    //clk_10ms input
	   
	always begin 
		
		
		clk_10ms<=1'b0;
      #10;
      clk_10ms<=1'b1;
		#10;
		
	end 
    //
    initial begin
			
			//初始設定
			 clear_button<=1'b1;
			 start_button<=1'b1;
			 reset<=1'b0;
			 //reset重制
            repeat(5)@(posedge clk_10ms)reset<=1'b1; //當有負緣clk_10ms時重複做5次 
			 reset<=1'b0;
			 //按下 start_button
			repeat(4)@(posedge clk_10ms)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			 start_button<=1'b1;
			 #100;
			  //按下 start_button
			repeat(4)@(posedge clk_10ms)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			start_button<=1'b1;
			#50;
			//按下 clear_button
			repeat(4)@(posedge clk_10ms)clear_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			clear_button<=1'b1;

			 //按下 start_button
 			repeat(4)@(posedge clk_10ms)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			start_button<=1'b1;
		
    end




	initial #700 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h%h %h %h ",clear_button,start_button,clk_10ms,reset,clear,clk,qout_T_FF);
endmodule
