// start_stop_timer_controller
module start_stop_timer_controller(
    input clear_button,start_button,clk_10ms,reset,
    output  clear,clk,
    output reg qout_T_FF
);   
wire clear_button_use_sw_out,start_button_use_sw_out;
//reg qout_T_FF;
//wire clockout_10ms;
	
	//module switch_debouncer(input clk_10ms,reset,sw_in,output reg [1:0] q_signal,output wire sw_out,j,k);
	 switch_debouncer clear_button_use(.clk_10ms(clk_10ms),.reset(reset),.sw_in(clear_button),.q_signal(),.sw_out(clear_button_use_sw_out),.j(),.k());
    switch_debouncer start_button_use(.clk_10ms(clk_10ms),.reset(reset),.sw_in(start_button),.q_signal(),.sw_out(start_button_use_sw_out),.j(),.k());
    
    //T-flip flop PART
    always @(posedge start_button_use_sw_out or posedge reset)
    begin
        if(reset) 
            qout_T_FF<=0;
        else 
            qout_T_FF<=~qout_T_FF;   
    end
    //generate output clear &clk signals
    assign clear=(clear_button_use_sw_out&~qout_T_FF)|reset;
    assign clk=clk_10ms&qout_T_FF;

    
endmodule