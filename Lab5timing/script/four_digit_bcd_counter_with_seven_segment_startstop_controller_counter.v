// four_digit_bcd_counter_with_seven_segment
module four_digit_bcd_counter_with_seven_segment_startstop_controller_counter(
    input clk_50MHz,clear_button,start_button,
    output  [6:0] HEX0,HEX1,HEX2,HEX3
	
);   
wire [3:0] qout0,qout1,qout2,qout3;
wire clockout_10ms;
wire clear_control_out,clk_control_out;	
    //變成10ms 訊號當clk
	 //module timing_clk_10ms(input clk_50MHZ,clear,output cout_n_5);   
	 timing_clk_10ms timing1(.clk_50MHZ(clk_50MHz),.clear(1'b0),.cout_n_5(clockout_10ms));
	 
     //開關控制
    //module start_stop_timer_controller(input clear_button,start_button,clk_10ms,reset,output  clear,clk,output reg qout_T_FF);   
    start_stop_timer_controller controller(.clear_button(clear_button),.start_button(start_button),
                                            .clk_10ms(clockout_10ms),.reset(1'b0),
                                            .clear(clear_control_out),.clk(clk_control_out),.qout_T_FF());

    //轉成碼表計數
    //module four_digit_bcd_counter(input clk,clear, output  [3:0] qout0,qout1,qout2,qout3);   
	 four_digit_bcd_counter counter_unit(.clk(clk_control_out),.clear(clear_control_out), 
                                         .qout0(qout0), .qout1(qout1),.qout2(qout2),.qout3(qout3));   	 
    
    //顯示到7段顯示器
	//module bin_7seg_decoder_version2(input [3:0] four_bits_binary,output reg [6:0]data_out_seven_segment);
    bin_7seg_decoder_version2 led0(.four_bits_binary(qout0),.data_out_seven_segment(HEX0));
    bin_7seg_decoder_version2 led1(.four_bits_binary(qout1),.data_out_seven_segment(HEX1));
	bin_7seg_decoder_version2 led2(.four_bits_binary(qout2),.data_out_seven_segment(HEX2));
    bin_7seg_decoder_version2 led3(.four_bits_binary(qout3),.data_out_seven_segment(HEX3));
    
endmodule