// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module timing_clk_10ms_tb;
// internal signals declaration

	reg clk_50MHZ,clear;
	wire [3:0] qout0,qout1,qout2,qout3,qout4,qout5;
	wire cout_n_5;
	
	//Unit Under Test instance and port map
	timing_clk_10ms UUT(.clk_50MHZ(clk_50MHZ),.clear(clear),.qout0(qout0),.qout1(qout1),.qout2(qout2),.qout3(qout3),.qout4(qout4),.qout5(qout5),.cout_n_5(cout_n_5));

	always begin //stimulus generation block
		
		#10;
		clk_50MHZ<=1'b0;
        #10;
        clk_50MHZ<=1'b1;
		
	end 
    initial begin
        #10 clear<=1'b0;
        #10 clear<=1'b1;
		  #10 clear<=1'b0;

    end
	initial #10000000 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h %h %h %h",clk_50MHZ,clear,qout0,qout1,qout2,qout3,qout4,qout5,cout_n_5);
endmodule
