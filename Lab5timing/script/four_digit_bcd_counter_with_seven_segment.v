// four_digit_bcd_counter_with_seven_segment
module four_digit_bcd_counter_with_seven_segment(
    input clk,clear,
    output  [6:0] HEX0,HEX1,HEX2,HEX3
	 //output  [3:0] qout0,qout1,qout2,qout3
);   
wire [3:0] qout0,qout1,qout2,qout3;
wire clockout_10ms;
	
	 //module timing_clk_10ms(input clk_50MHZ,clear,output cout_n_5);   
	 //timing_clk_10ms timing1(.clk_50MHZ(clk),.clear(clear),.cout_n_5(clockout_10ms));
	 
    //module four_digit_bcd_counter(input clk,clear, output  [3:0] qout0,qout1,qout2,qout3);   
    //four_digit_bcd_counter counter_unit(.clk(clockout_10ms),.clear(clear), .qout0(qout0),.qout1(qout1),.qout2(qout2),.qout3(qout3)); 
	 four_digit_bcd_counter counter_unit(.clk(clk),.clear(clear), .qout0(qout0),.qout1(qout1),.qout2(qout2),.qout3(qout3));   	 
   
	//module bin_7seg_decoder_version2(input [3:0] four_bits_binary,output reg [6:0]data_out_seven_segment);
    bin_7seg_decoder_version2 led0(.four_bits_binary(qout0),.data_out_seven_segment(HEX0));
    bin_7seg_decoder_version2 led1(.four_bits_binary(qout1),.data_out_seven_segment(HEX1));
	 bin_7seg_decoder_version2 led2(.four_bits_binary(qout2),.data_out_seven_segment(HEX2));
    bin_7seg_decoder_version2 led3(.four_bits_binary(qout3),.data_out_seven_segment(HEX3));
    
endmodule