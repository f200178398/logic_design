// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module four_digit_bcd_counter_tb;
// internal signals declaration

	reg clk,clear;
	wire [3:0] qout0,qout1,qout2,qout3;
	wire  cout_n_0,cout_n_1,cout_n_2,cout_n_3;
	
	//Unit Under Test instance and port map
	four_digit_bcd_counter UUT(.clk(clk),.clear(clear),.qout0(qout0),.qout1(qout1),.qout2(qout2),.qout3(qout3),.cout_n_0(cout_n_0),.cout_n_1(cout_n_1),.cout_n_2(cout_n_2),.cout_n_3(cout_n_3));

	always begin //stimulus generation block
		
		#10;
		clk<=1'b0;
        #10;
        clk<=1'b1;
		
	end 
    initial begin
        #10 clear<=1'b0;
        #10 clear<=1'b1;
		  #10 clear<=1'b0;

    end
	initial #100000 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h %h %h%h %h %h %h",clk,clear,qout0,qout1,qout2,qout3,cout_n_0,cout_n_1,cout_n_2,cout_n_3);
endmodule
