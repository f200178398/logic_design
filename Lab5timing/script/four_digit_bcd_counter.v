
module four_digit_bcd_counter(
    input clk,clear,
    output  [3:0] qout0,qout1,qout2,qout3,
	 output  cout_n_0,cout_n_1,cout_n_2,cout_n_3
);   
//wire cout_n_0,cout_n_1,cout_n_2,cout_n_3;
	//module mod10_up_clk(input clk,reset_n,output reg [3:0] qout,output cout_n);
   mod10_up_clk cnt0(.clk(clk)     ,.reset_n(clear),.qout(qout0),.cout_n(cout_n_0));
	mod10_up_clk cnt1(.clk(cout_n_0),.reset_n(clear),.qout(qout1),.cout_n(cout_n_1));
	mod10_up_clk cnt2(.clk(cout_n_1),.reset_n(clear),.qout(qout2),.cout_n(cout_n_2));
	// module mod6_up_clk(input clk,reset_n,output reg [3:0] qout,output cout_n);
   mod6_up_clk  cnt3(.clk(cout_n_2),.reset_n(clear),.qout(qout3),.cout_n(cout_n_3));
    
endmodule