// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module four_digit_bcd_counter_with_seven_segment_startstop_controller_tb;
// internal signals declaration
	 
	
	reg clk_10ms,clear_button,start_button,reset;
    //wire [6:0] HEX0,HEX1,HEX2,HEX3;
	 wire  [3:0] qout0,qout1,qout2,qout3;
	//Unit Under Test instance and port map
	//four_digit_bcd_counter_with_seven_segment_startstop_controller UUT(.reset(reset),.clk(clk),.clear_button(clear_button),.start_button(start_button),.HEX0(HEX0),.HEX1(HEX1),.HEX2(HEX2),.HEX3(HEX3));
    four_digit_bcd_counter_with_seven_segment_startstop_controller UUT(.reset(reset),.clk_10ms(clk_10ms),.clear_button(clear_button),.start_button(start_button),.qout0(qout0),.qout1(qout1),.qout2(qout2),.qout3(qout3));
    //clk_10ms input
	   
	always begin 
		
		
	    clk_10ms<=1'b0;
      #10;
        clk_10ms<=1'b1;
	  #10;
		
	end 
    //
    // initial begin
			
	// 		///初始設定
	// 		 clear_button<=1'b1;
	// 		 start_button<=1'b1;
	// 		 reset<=1'b0;
	// 		 //reset重制
    //       repeat(5)@(posedge clk)reset<=1'b1; //當有負緣clk_10ms時重複做5次 
	// 		 reset<=1'b0;
			
	// 		 //按下 start_button
	// 		repeat(4)@(posedge clk)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
	// 		 start_button<=1'b1;
	// 		 #100;
	// 		  //按下 start_button
	// 		repeat(4)@(posedge clk)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
	// 		start_button<=1'b1;
	// 		#50;
	// 		//按下 clear_button
	// 		//repeat(4)@(posedge clk)clear_button<=1'b0; //當有負緣clk_10ms時重複做5次 
	// 		//clear_button<=1'b1;

	// 		 //按下 start_button
 	// 		repeat(4)@(posedge clk)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
	// 		start_button<=1'b1;
		
    // end
     initial begin
			///初始設定
			start_button<=1'b1;
			clear_button<=1'b1;
			reset<=1'b0;
	  
			#10 reset<=1'b1;
			#100 reset<=1'b0;

    end

    initial begin
			
			#150;
			repeat(7)@(posedge clk_10ms)clear_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			 clear_button<=1'b1;
			 #100;
			
			#500;
			 //按下 start_button
			repeat(7)@(posedge clk_10ms)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			 start_button<=1'b1;
			 #100;
			 start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			 #200 
			 start_button<=1'b1;
			 #150;
			repeat(4)@(posedge clk_10ms)clear_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			 clear_button<=1'b1;
			 
			 #400;
			 //按下 start_button
			repeat(7)@(posedge clk_10ms)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			 start_button<=1'b1;
			  //按下 start_button
			// repeat(4)@(posedge clk_10ms)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			// start_button<=1'b1;
			// #50;
		
			 //按下 start_button
 			//repeat(4)@(posedge clk)start_button<=1'b0; //當有負緣clk_10ms時重複做5次 
			//start_button<=1'b1;
		
    end



	initial #100000 $finish;
	initial  //response monitoring block
		//$monitor($realtime,"ns %h %h %h %h %h%h %h %h ",reset,clear_button,start_button,clk,HEX0,HEX1,HEX2,HEX3);
		$monitor($realtime,"ns %h %h %h %h %h%h %h %h ",reset,clear_button,start_button,clk_10ms,qout0,qout1,qout2,qout3);
endmodule
