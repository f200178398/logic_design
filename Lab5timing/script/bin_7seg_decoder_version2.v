// a  binary to7 segment decoder
module bin_7seg_decoder_version2(
    input [3:0] four_bits_binary,
    output reg [6:0]data_out_seven_segment
);



//body of binary to7 segment decoder
always @(*)
    begin
      case(four_bits_binary)
        4'b0000: data_out_seven_segment = 7'b100_0000;
        4'b0001: data_out_seven_segment = 7'b111_1001;
        4'b0010: data_out_seven_segment = 7'b010_0100;
        4'b0011: data_out_seven_segment = 7'b011_0000;

        4'b0100: data_out_seven_segment = 7'b001_1001;
        4'b0101: data_out_seven_segment = 7'b001_0010;
        4'b0110: data_out_seven_segment = 7'b000_0010;
        4'b0111: data_out_seven_segment = 7'b111_1000;

        4'b1000: data_out_seven_segment = 7'b000_0000;
        4'b1001: data_out_seven_segment = 7'b001_0000;
        4'b1010: data_out_seven_segment = 7'b000_1000;
        4'b1011: data_out_seven_segment = 7'b000_0011;

        4'b1100: data_out_seven_segment = 7'b100_0110;
        4'b1101: data_out_seven_segment = 7'b010_0001;
        4'b1110: data_out_seven_segment = 7'b000_0110;
        4'b1111: data_out_seven_segment = 7'b000_1110;
        
      endcase

    end
endmodule