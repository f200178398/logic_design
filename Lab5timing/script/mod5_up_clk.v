// a mod 5 clk
module mod5_up_clk(
    input clk,reset_n,
    
    output reg [3:0] qout,
    output cout_n
);


assign cout_n=~(qout==4);
//body of mod 5 clk
always @(posedge clk or posedge reset_n)
    begin
        if(reset_n) 
            qout<=0;
			else if(!cout_n)
				qout<=0;
        else
            qout<=qout+4'b0001;

    end
endmodule