
module timing_clk_10ms(
    input clk_50MHZ,clear,
    output  [3:0] qout0,qout1,qout2,qout3,qout4,qout5,
	 output cout_n_5
	 
);   
wire cout_n_0,cout_n_1,cout_n_2,cout_n_3,cout_n_4;
//module mod10_up_clk(input clk,reset_n,output reg [3:0] qout,output cout_n);

   mod10_up_clk cnt0(.clk(clk_50MHZ)     ,.reset_n(clear),.qout(qout0),.cout_n(cout_n_0));
	mod10_up_clk cnt1(.clk(cout_n_0),.reset_n(clear),.qout(qout1),.cout_n(cout_n_1));
	mod10_up_clk cnt2(.clk(cout_n_1),.reset_n(clear),.qout(qout2),.cout_n(cout_n_2));
	mod10_up_clk cnt3(.clk(cout_n_2),.reset_n(clear),.qout(qout3),.cout_n(cout_n_3));
	mod10_up_clk cnt4(.clk(cout_n_3),.reset_n(clear),.qout(qout4),.cout_n(cout_n_4));
   mod5_up_clk cnt5(.clk(cout_n_4),.reset_n(clear),.qout(qout5),.cout_n(cout_n_5));
	
endmodule