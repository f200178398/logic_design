// a mod 10 clk
module mod10_up_clk(
    input clk,reset_n,
    
    output reg [3:0] qout,
    output cout_n
);
wire clear_n;

assign cout_n = ~(qout[3]);
assign clear_n= (qout == 9);
//body of mod 10 clk
always @(posedge clk or posedge reset_n)
    begin
        if(reset_n) 
            qout<=0;
        else if(clear_n)
            qout<=0;
        else
            qout<=qout+1;
    end
endmodule
