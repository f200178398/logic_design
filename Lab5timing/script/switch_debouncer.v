// switch debouncer
module switch_debouncer(
    input clk_10ms,reset,sw_in,
	 output reg [1:0] q_signal,
    output wire sw_out,j,k
);
wire d;
//reg[1:0] q_signal;
reg q_jk;

// two d flip flop to detect the signals
    always @(posedge clk_10ms or posedge reset)
        begin
            if(reset) 
                q_signal<=0;
 
            else
                q_signal[1]<=q_signal[0];
                q_signal[0]<=~sw_in;

        end
   assign j=(q_signal[0]&q_signal[1]);
	assign k=~(q_signal[0]|q_signal[1]);
    assign d=(((q_signal[0]&q_signal[1])&(~q_jk))|((q_signal[0]|q_signal[1])&(q_jk)));
	
    // use jk flip flop to generate a signal
    always @(posedge clk_10ms or posedge reset)
        begin
            if(reset) 
                q_jk<=0;
            else
                q_jk<=d;

        end
    assign sw_out=q_jk;
    
endmodule