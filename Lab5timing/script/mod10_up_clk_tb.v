// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module mod10_up_clk_tb;
// internal signals declaration

    reg clk,reset_n;
    wire [3:0] qout;
    wire cout_n;
    //Unit Under Test instance and port map
    mod10_up_clk UUT(.clk(clk),.reset_n(reset_n),.qout(qout),.cout_n(cout_n));

    always begin //stimulus generation block
        
        #10;
        clk<=1'b0;
        #10;
        clk<=1'b1;
        
    end 
    initial begin
        reset_n<=1'b0;
        #50 reset_n<=1'b1;
          #50 reset_n<=1'b0;
          #500 reset_n<=1'b1;
          #50 reset_n<=1'b0;

    end
    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h %h",clk,reset_n,qout,cout_n);
endmodule

