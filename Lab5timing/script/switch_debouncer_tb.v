// a test bench for the switch_debouncer
`timescale 1 ns/100 ps
module switch_debouncer_tb;
// internal signals declaration

	reg clk_10ms,reset,sw_in;
	wire sw_out,j,k;
	wire [1:0] q_signal;
	integer i;
	//Unit Under Test instance and port map
	switch_debouncer UUT(.clk_10ms(clk_10ms),.reset(reset),.sw_in(sw_in),.sw_out(sw_out),.q_signal(q_signal),.j(j),.k(k));

	always begin //stimulus generation block
		
		
		clk_10ms<=1'b0;
      #10;
      clk_10ms<=1'b1;
		#10;
		
	end 
    initial begin
         reset<=1'b0;
         sw_in<=1'b1;
         repeat(5)@(negedge clk_10ms) //當有負緣clk_10ms時重複做5次
            reset<=1'b1;
         reset<=1'b0;
		for(i=0;i<2;i=i+1)
            begin
               //sw_out one signal 
				// #7 sw_in<=1'b0;
            //    #6 sw_in<=1'b1;
            //    #1 sw_in<=1'b0;
            //    #1 sw_in<=1'b1;
            //    #2 sw_in<=1'b0;
            //    #2 sw_in<=1'b1;
                //sw_out no signal 
               //  #5 sw_in<=1'b0;
               //  #10 sw_in<=1'b1;
               //  #1 sw_in<=1'b0;
               //  #1 sw_in<=1'b1;
               //  #2 sw_in<=1'b0;
               //  #2 sw_in<=1'b1;
               #9.9 sw_in<=1'b0;
               #0.2 sw_in<=1'b1;
               #1 sw_in<=1'b0;
               #1 sw_in<=1'b1;
               #2 sw_in<=1'b0;
               #2 sw_in<=1'b1;
	       
            end
				
    end




	initial #700 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h%h %h %h ",clk_10ms,reset,sw_in,sw_out,q_signal,j,k);
endmodule
