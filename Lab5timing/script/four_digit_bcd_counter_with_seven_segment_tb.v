// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module four_digit_bcd_counter_with_seven_segment_tb;
// internal signals declaration

	reg clk,clear;
	wire [6:0] HEX0,HEX1,HEX2,HEX3;

	
	//Unit Under Test instance and port map
	four_digit_bcd_counter_with_seven_segment UUT(.clk(clk),.clear(clear),.HEX0(HEX0),.HEX1(HEX1),.HEX2(HEX2),.HEX3(HEX3));
	always begin //stimulus generation block
		
		#10;
		clk<=1'b0;
        #10;
        clk<=1'b1;
		
	end 
    initial begin
        #10 clear<=1'b0;
        #10 clear<=1'b1;
		  #10 clear<=1'b0;
		 
    end
	initial #1000000 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h %h %h",clk,clear,HEX0,HEX1,HEX2,HEX3);
endmodule
