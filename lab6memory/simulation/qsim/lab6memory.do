onerror {exit -code 1}
vlib work
vlog -work work Lab6Memory.vo
vlog -work work RF_plus_ALU.vwf.vt
vsim -novopt -c -t 1ps -L cycloneive_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.RF_plus_ALU_vlg_vec_tst -voptargs="+acc"
vcd file -direction Lab6Memory.msim.vcd
vcd add -internal RF_plus_ALU_vlg_vec_tst/*
vcd add -internal RF_plus_ALU_vlg_vec_tst/i1/*
run -all
quit -f
