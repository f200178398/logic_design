library verilog;
use verilog.vl_types.all;
entity ALU_Adder_Subtractor_16bit_vlg_sample_tst is
    port(
        a               : in     vl_logic_vector(15 downto 0);
        b               : in     vl_logic_vector(15 downto 0);
        c_in            : in     vl_logic;
        mode            : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end ALU_Adder_Subtractor_16bit_vlg_sample_tst;
