library verilog;
use verilog.vl_types.all;
entity decoder_3to8_vlg_sample_tst is
    port(
        WE              : in     vl_logic;
        X0              : in     vl_logic;
        X1              : in     vl_logic;
        X2              : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end decoder_3to8_vlg_sample_tst;
