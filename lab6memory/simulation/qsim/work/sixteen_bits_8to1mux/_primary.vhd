library verilog;
use verilog.vl_types.all;
entity sixteen_bits_8to1mux is
    port(
        Y               : out    vl_logic_vector(15 downto 0);
        selection2      : in     vl_logic;
        selection0      : in     vl_logic;
        selection1      : in     vl_logic;
        A0              : in     vl_logic_vector(15 downto 0);
        A1              : in     vl_logic_vector(15 downto 0);
        A2              : in     vl_logic_vector(15 downto 0);
        A3              : in     vl_logic_vector(15 downto 0);
        A4              : in     vl_logic_vector(15 downto 0);
        A5              : in     vl_logic_vector(15 downto 0);
        A6              : in     vl_logic_vector(15 downto 0);
        A7              : in     vl_logic_vector(15 downto 0)
    );
end sixteen_bits_8to1mux;
