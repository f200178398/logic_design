library verilog;
use verilog.vl_types.all;
entity ALU_Adder_Subtractor_16bit is
    port(
        sum             : out    vl_logic_vector(15 downto 0);
        c_in            : in     vl_logic;
        a               : in     vl_logic_vector(15 downto 0);
        mode            : in     vl_logic;
        b               : in     vl_logic_vector(15 downto 0)
    );
end ALU_Adder_Subtractor_16bit;
