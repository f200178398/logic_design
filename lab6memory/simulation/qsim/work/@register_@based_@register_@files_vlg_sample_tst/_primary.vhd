library verilog;
use verilog.vl_types.all;
entity Register_Based_Register_Files_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        RA_addr         : in     vl_logic_vector(2 downto 0);
        RB_addr         : in     vl_logic_vector(2 downto 0);
        WE              : in     vl_logic;
        WR_addr         : in     vl_logic_vector(2 downto 0);
        WR_data         : in     vl_logic_vector(15 downto 0);
        sampler_tx      : out    vl_logic
    );
end Register_Based_Register_Files_vlg_sample_tst;
