library verilog;
use verilog.vl_types.all;
entity ALU_Adder_Subtractor_16bit_vlg_check_tst is
    port(
        sum             : in     vl_logic_vector(15 downto 0);
        sampler_rx      : in     vl_logic
    );
end ALU_Adder_Subtractor_16bit_vlg_check_tst;
