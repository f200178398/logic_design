library verilog;
use verilog.vl_types.all;
entity decoder_3to8 is
    port(
        Y0              : out    vl_logic;
        WE              : in     vl_logic;
        X0              : in     vl_logic;
        X1              : in     vl_logic;
        X2              : in     vl_logic;
        Y1              : out    vl_logic;
        Y2              : out    vl_logic;
        Y3              : out    vl_logic;
        Y7              : out    vl_logic;
        Y6              : out    vl_logic;
        Y5              : out    vl_logic;
        Y4              : out    vl_logic
    );
end decoder_3to8;
