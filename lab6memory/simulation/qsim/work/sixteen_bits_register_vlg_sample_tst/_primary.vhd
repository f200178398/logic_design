library verilog;
use verilog.vl_types.all;
entity sixteen_bits_register_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        din             : in     vl_logic_vector(15 downto 0);
        load            : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end sixteen_bits_register_vlg_sample_tst;
