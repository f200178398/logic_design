library verilog;
use verilog.vl_types.all;
entity Register_Based_Register_Files_vlg_check_tst is
    port(
        RA_data         : in     vl_logic_vector(15 downto 0);
        RB_data         : in     vl_logic_vector(15 downto 0);
        sampler_rx      : in     vl_logic
    );
end Register_Based_Register_Files_vlg_check_tst;
