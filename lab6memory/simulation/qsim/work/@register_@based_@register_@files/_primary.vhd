library verilog;
use verilog.vl_types.all;
entity Register_Based_Register_Files is
    port(
        RA_data         : out    vl_logic_vector(15 downto 0);
        RA_addr         : in     vl_logic_vector(2 downto 0);
        clk             : in     vl_logic;
        WR_addr         : in     vl_logic_vector(2 downto 0);
        WE              : in     vl_logic;
        WR_data         : in     vl_logic_vector(15 downto 0);
        RB_data         : out    vl_logic_vector(15 downto 0);
        RB_addr         : in     vl_logic_vector(2 downto 0)
    );
end Register_Based_Register_Files;
