library verilog;
use verilog.vl_types.all;
entity sixteen_bits_register is
    port(
        qout            : out    vl_logic_vector(15 downto 0);
        clk             : in     vl_logic;
        din             : in     vl_logic_vector(15 downto 0);
        load            : in     vl_logic
    );
end sixteen_bits_register;
