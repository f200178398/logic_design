transcript on
if {[file exists gate_work]} {
	vdel -lib gate_work -all
}
vlib gate_work
vmap work gate_work

vlog -vlog01compat -work work +incdir+. {Lab6Memory_7_1200mv_85c_slow.vo}

vlog -vlog01compat -work work +incdir+D:/DigitalDesignlabs/Lab6Memory/script {D:/DigitalDesignlabs/Lab6Memory/script/memory_module_verilog_tb.v}

vsim -t 1ps +transport_int_delays +transport_path_delays -L altera_ver -L cycloneive_ver -L gate_work -L work -voptargs="+acc"  memory_module_verilog_tb

add wave *
view structure
view signals
run -all
