library verilog;
use verilog.vl_types.all;
entity memory_module_vlg_sample_tst is
    port(
        ALU_out         : in     vl_logic_vector(15 downto 0);
        clk             : in     vl_logic;
        ext_addr        : in     vl_logic_vector(15 downto 0);
        ext_data        : in     vl_logic_vector(15 downto 0);
        ext_we          : in     vl_logic;
        maddr_sel       : in     vl_logic;
        mem_we          : in     vl_logic;
        PC_out          : in     vl_logic_vector(15 downto 0);
        test_normal     : in     vl_logic;
        WDR_out         : in     vl_logic_vector(15 downto 0);
        sampler_tx      : out    vl_logic
    );
end memory_module_vlg_sample_tst;
