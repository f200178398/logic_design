library verilog;
use verilog.vl_types.all;
entity SE_8bits_to_16_bits_vlg_check_tst is
    port(
        b_in            : in     vl_logic_vector(15 downto 0);
        sampler_rx      : in     vl_logic
    );
end SE_8bits_to_16_bits_vlg_check_tst;
