library verilog;
use verilog.vl_types.all;
entity memory_module_vlg_check_tst is
    port(
        mem_out         : in     vl_logic_vector(15 downto 0);
        sampler_rx      : in     vl_logic
    );
end memory_module_vlg_check_tst;
