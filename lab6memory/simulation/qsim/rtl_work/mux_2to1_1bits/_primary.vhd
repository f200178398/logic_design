library verilog;
use verilog.vl_types.all;
entity mux_2to1_1bits is
    port(
        A               : in     vl_logic;
        B               : in     vl_logic;
        sel             : in     vl_logic;
        Y               : out    vl_logic
    );
end mux_2to1_1bits;
