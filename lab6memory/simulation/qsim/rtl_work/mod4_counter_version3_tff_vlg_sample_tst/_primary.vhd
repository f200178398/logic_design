library verilog;
use verilog.vl_types.all;
entity mod4_counter_version3_tff_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        din             : in     vl_logic_vector(1 downto 0);
        en              : in     vl_logic;
        load            : in     vl_logic;
        reset_n         : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end mod4_counter_version3_tff_vlg_sample_tst;
