library verilog;
use verilog.vl_types.all;
entity mod4_counter_version2 is
    port(
        Z               : out    vl_logic;
        reset           : in     vl_logic;
        clk             : in     vl_logic;
        x               : in     vl_logic;
        din             : in     vl_logic_vector(1 downto 0);
        load            : in     vl_logic;
        yout            : out    vl_logic_vector(1 downto 0)
    );
end mod4_counter_version2;
