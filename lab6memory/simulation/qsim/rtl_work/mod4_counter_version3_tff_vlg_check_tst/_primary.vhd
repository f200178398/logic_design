library verilog;
use verilog.vl_types.all;
entity mod4_counter_version3_tff_vlg_check_tst is
    port(
        cout            : in     vl_logic;
        qout            : in     vl_logic_vector(1 downto 0);
        sampler_rx      : in     vl_logic
    );
end mod4_counter_version3_tff_vlg_check_tst;
