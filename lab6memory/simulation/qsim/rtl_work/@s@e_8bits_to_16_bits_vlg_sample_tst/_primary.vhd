library verilog;
use verilog.vl_types.all;
entity SE_8bits_to_16_bits_vlg_sample_tst is
    port(
        ain             : in     vl_logic_vector(7 downto 0);
        sampler_tx      : out    vl_logic
    );
end SE_8bits_to_16_bits_vlg_sample_tst;
