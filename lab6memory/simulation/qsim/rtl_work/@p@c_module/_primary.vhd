library verilog;
use verilog.vl_types.all;
entity PC_module is
    port(
        PC_out          : out    vl_logic_vector(15 downto 0);
        clk             : in     vl_logic;
        reset_n         : in     vl_logic;
        PC_ld           : in     vl_logic;
        PC_inc          : in     vl_logic;
        PC_src          : in     vl_logic_vector(1 downto 0);
        IR10to0         : in     vl_logic_vector(10 downto 0);
        Rmd_out         : in     vl_logic_vector(15 downto 0)
    );
end PC_module;
