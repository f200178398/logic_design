library verilog;
use verilog.vl_types.all;
entity mod4_counter_version2_vlg_check_tst is
    port(
        yout            : in     vl_logic_vector(1 downto 0);
        Z               : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end mod4_counter_version2_vlg_check_tst;
