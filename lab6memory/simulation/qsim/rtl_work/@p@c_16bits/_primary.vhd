library verilog;
use verilog.vl_types.all;
entity PC_16bits is
    port(
        qout            : out    vl_logic_vector(15 downto 0);
        en              : in     vl_logic;
        clk             : in     vl_logic;
        load            : in     vl_logic;
        reset_n         : in     vl_logic;
        din             : in     vl_logic_vector(15 downto 0)
    );
end PC_16bits;
