library verilog;
use verilog.vl_types.all;
entity ALU_module is
    port(
        C               : out    vl_logic;
        ALU_func        : in     vl_logic;
        sub_add         : in     vl_logic;
        clr_C           : in     vl_logic;
        Rmd_out         : in     vl_logic_vector(15 downto 0);
        ALU_src         : in     vl_logic;
        Rn_out          : in     vl_logic_vector(15 downto 0);
        IR40            : in     vl_logic_vector(4 downto 0);
        clr_IR40        : in     vl_logic;
        clk             : in     vl_logic;
        flag_ld         : in     vl_logic;
        V               : out    vl_logic;
        N               : out    vl_logic;
        Z               : out    vl_logic;
        ALU_out         : out    vl_logic_vector(15 downto 0)
    );
end ALU_module;
