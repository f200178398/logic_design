library verilog;
use verilog.vl_types.all;
entity RF_module_vlg_check_tst is
    port(
        Rmd_out         : in     vl_logic_vector(15 downto 0);
        Rn_out          : in     vl_logic_vector(15 downto 0);
        sampler_rx      : in     vl_logic
    );
end RF_module_vlg_check_tst;
