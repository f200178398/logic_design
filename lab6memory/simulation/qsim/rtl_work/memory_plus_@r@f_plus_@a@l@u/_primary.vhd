library verilog;
use verilog.vl_types.all;
entity memory_plus_RF_plus_ALU is
    port(
        mem_out         : out    vl_logic_vector(15 downto 0);
        maddr_sel       : in     vl_logic;
        clk             : in     vl_logic;
        mem_we          : in     vl_logic;
        ext_we          : in     vl_logic;
        test_normal     : in     vl_logic;
        ALU_func        : in     vl_logic;
        RF_we           : in     vl_logic;
        IR_ld           : in     vl_logic;
        Rd_sel          : in     vl_logic;
        ALU_src         : in     vl_logic;
        clr_IR40        : in     vl_logic;
        ext_addr        : in     vl_logic_vector(15 downto 0);
        ext_data        : in     vl_logic_vector(15 downto 0);
        PC_out          : in     vl_logic_vector(15 downto 0);
        WDR_out         : in     vl_logic_vector(15 downto 0);
        Rm_out          : out    vl_logic_vector(15 downto 0)
    );
end memory_plus_RF_plus_ALU;
