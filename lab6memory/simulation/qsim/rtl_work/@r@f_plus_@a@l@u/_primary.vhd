library verilog;
use verilog.vl_types.all;
entity RF_plus_ALU is
    port(
        V               : out    vl_logic;
        ALU_func        : in     vl_logic;
        RF_WE           : in     vl_logic;
        ext_WE          : in     vl_logic;
        test_normal     : in     vl_logic;
        clk             : in     vl_logic;
        RA_addr         : in     vl_logic_vector(2 downto 0);
        RB_addr         : in     vl_logic_vector(2 downto 0);
        WR_addr         : in     vl_logic_vector(2 downto 0);
        ext_addr        : in     vl_logic_vector(2 downto 0);
        ext_data        : in     vl_logic_vector(15 downto 0);
        N               : out    vl_logic;
        C               : out    vl_logic;
        Z               : out    vl_logic;
        RA_data         : out    vl_logic_vector(15 downto 0);
        RB_data         : out    vl_logic_vector(15 downto 0)
    );
end RF_plus_ALU;
