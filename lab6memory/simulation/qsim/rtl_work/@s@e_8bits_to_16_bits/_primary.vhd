library verilog;
use verilog.vl_types.all;
entity SE_8bits_to_16_bits is
    port(
        b_in            : out    vl_logic_vector(15 downto 0);
        ain             : in     vl_logic_vector(7 downto 0)
    );
end SE_8bits_to_16_bits;
