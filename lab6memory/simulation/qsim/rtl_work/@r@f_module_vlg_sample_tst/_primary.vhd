library verilog;
use verilog.vl_types.all;
entity RF_module_vlg_sample_tst is
    port(
        ALU_out         : in     vl_logic_vector(15 downto 0);
        clk             : in     vl_logic;
        IR10to0         : in     vl_logic_vector(10 downto 0);
        LH_sel          : in     vl_logic;
        mem_out         : in     vl_logic_vector(15 downto 0);
        PC_out          : in     vl_logic_vector(15 downto 0);
        Raa_sel         : in     vl_logic;
        Rd_sel          : in     vl_logic_vector(1 downto 0);
        RF_we           : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end RF_module_vlg_sample_tst;
