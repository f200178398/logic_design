library verilog;
use verilog.vl_types.all;
entity RF_module is
    port(
        Rmd_out         : out    vl_logic_vector(15 downto 0);
        RF_we           : in     vl_logic;
        clk             : in     vl_logic;
        Raa_sel         : in     vl_logic;
        IR10to0         : in     vl_logic_vector(10 downto 0);
        Rd_sel          : in     vl_logic_vector(1 downto 0);
        ALU_out         : in     vl_logic_vector(15 downto 0);
        mem_out         : in     vl_logic_vector(15 downto 0);
        LH_sel          : in     vl_logic;
        PC_out          : in     vl_logic_vector(15 downto 0);
        Rn_out          : out    vl_logic_vector(15 downto 0)
    );
end RF_module;
