library verilog;
use verilog.vl_types.all;
entity mod4_counter_version3_tff is
    port(
        cout            : out    vl_logic;
        en              : in     vl_logic;
        reset_n         : in     vl_logic;
        din             : in     vl_logic_vector(1 downto 0);
        load            : in     vl_logic;
        clk             : in     vl_logic;
        qout            : out    vl_logic_vector(1 downto 0)
    );
end mod4_counter_version3_tff;
