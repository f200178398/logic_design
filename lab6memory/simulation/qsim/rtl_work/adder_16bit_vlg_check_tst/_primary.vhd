library verilog;
use verilog.vl_types.all;
entity adder_16bit_vlg_check_tst is
    port(
        sum             : in     vl_logic_vector(15 downto 0);
        sampler_rx      : in     vl_logic
    );
end adder_16bit_vlg_check_tst;
