library verilog;
use verilog.vl_types.all;
entity datapath_vlg_check_tst is
    port(
        C               : in     vl_logic;
        IR              : in     vl_logic_vector(15 downto 0);
        mem_out         : in     vl_logic_vector(15 downto 0);
        N               : in     vl_logic;
        OutR            : in     vl_logic_vector(15 downto 0);
        V               : in     vl_logic;
        Z               : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end datapath_vlg_check_tst;
