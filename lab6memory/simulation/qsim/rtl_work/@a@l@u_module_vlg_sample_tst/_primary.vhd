library verilog;
use verilog.vl_types.all;
entity ALU_module_vlg_sample_tst is
    port(
        ALU_func        : in     vl_logic;
        ALU_src         : in     vl_logic;
        clk             : in     vl_logic;
        clr_C           : in     vl_logic;
        clr_IR40        : in     vl_logic;
        flag_ld         : in     vl_logic;
        IR40            : in     vl_logic_vector(4 downto 0);
        Rmd_out         : in     vl_logic_vector(15 downto 0);
        Rn_out          : in     vl_logic_vector(15 downto 0);
        sub_add         : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end ALU_module_vlg_sample_tst;
