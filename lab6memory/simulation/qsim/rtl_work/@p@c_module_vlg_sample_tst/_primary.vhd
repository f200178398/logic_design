library verilog;
use verilog.vl_types.all;
entity PC_module_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        IR10to0         : in     vl_logic_vector(10 downto 0);
        PC_inc          : in     vl_logic;
        PC_ld           : in     vl_logic;
        PC_src          : in     vl_logic_vector(1 downto 0);
        reset_n         : in     vl_logic;
        Rmd_out         : in     vl_logic_vector(15 downto 0);
        sampler_tx      : out    vl_logic
    );
end PC_module_vlg_sample_tst;
