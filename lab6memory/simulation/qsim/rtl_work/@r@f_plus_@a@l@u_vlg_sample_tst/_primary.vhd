library verilog;
use verilog.vl_types.all;
entity RF_plus_ALU_vlg_sample_tst is
    port(
        ALU_func        : in     vl_logic;
        clk             : in     vl_logic;
        ext_addr        : in     vl_logic_vector(2 downto 0);
        ext_data        : in     vl_logic_vector(15 downto 0);
        ext_WE          : in     vl_logic;
        RA_addr         : in     vl_logic_vector(2 downto 0);
        RB_addr         : in     vl_logic_vector(2 downto 0);
        RF_WE           : in     vl_logic;
        test_normal     : in     vl_logic;
        WR_addr         : in     vl_logic_vector(2 downto 0);
        sampler_tx      : out    vl_logic
    );
end RF_plus_ALU_vlg_sample_tst;
