library verilog;
use verilog.vl_types.all;
entity mod4_counter_version2_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        din             : in     vl_logic_vector(1 downto 0);
        load            : in     vl_logic;
        reset           : in     vl_logic;
        x               : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end mod4_counter_version2_vlg_sample_tst;
