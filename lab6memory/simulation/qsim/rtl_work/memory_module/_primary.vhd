library verilog;
use verilog.vl_types.all;
entity memory_module is
    port(
        mem_out         : out    vl_logic_vector(15 downto 0);
        clk             : in     vl_logic;
        mem_we          : in     vl_logic;
        ext_we          : in     vl_logic;
        test_normal     : in     vl_logic;
        maddr_sel       : in     vl_logic;
        ALU_out         : in     vl_logic_vector(15 downto 0);
        PC_out          : in     vl_logic_vector(15 downto 0);
        ext_addr        : in     vl_logic_vector(15 downto 0);
        WDR_out         : in     vl_logic_vector(15 downto 0);
        ext_data        : in     vl_logic_vector(15 downto 0)
    );
end memory_module;
