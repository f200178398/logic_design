library verilog;
use verilog.vl_types.all;
entity mod4_counter_vlg_check_tst is
    port(
        cout            : in     vl_logic;
        yout            : in     vl_logic_vector(1 downto 0);
        sampler_rx      : in     vl_logic
    );
end mod4_counter_vlg_check_tst;
