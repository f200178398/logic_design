library verilog;
use verilog.vl_types.all;
entity RF_plus_ALU_vlg_check_tst is
    port(
        C               : in     vl_logic;
        N               : in     vl_logic;
        RA_data         : in     vl_logic_vector(15 downto 0);
        RB_data         : in     vl_logic_vector(15 downto 0);
        V               : in     vl_logic;
        Z               : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end RF_plus_ALU_vlg_check_tst;
