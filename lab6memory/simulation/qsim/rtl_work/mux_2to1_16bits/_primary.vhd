library verilog;
use verilog.vl_types.all;
entity mux_2to1_16bits is
    port(
        A               : in     vl_logic_vector(15 downto 0);
        B               : in     vl_logic_vector(15 downto 0);
        sel             : in     vl_logic;
        Y               : out    vl_logic_vector(15 downto 0)
    );
end mux_2to1_16bits;
