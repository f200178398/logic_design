library verilog;
use verilog.vl_types.all;
entity memory_mf_2p_256by16bits is
    port(
        clock           : in     vl_logic;
        data            : in     vl_logic_vector(15 downto 0);
        addr            : in     vl_logic_vector(7 downto 0);
        wren            : in     vl_logic;
        q               : out    vl_logic_vector(15 downto 0)
    );
end memory_mf_2p_256by16bits;
