library verilog;
use verilog.vl_types.all;
entity adder_16bit is
    port(
        sum             : out    vl_logic_vector(15 downto 0);
        x_in            : in     vl_logic_vector(15 downto 0);
        y_in            : in     vl_logic_vector(15 downto 0)
    );
end adder_16bit;
