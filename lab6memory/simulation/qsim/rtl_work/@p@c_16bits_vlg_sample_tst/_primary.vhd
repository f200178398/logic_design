library verilog;
use verilog.vl_types.all;
entity PC_16bits_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        din             : in     vl_logic_vector(15 downto 0);
        en              : in     vl_logic;
        load            : in     vl_logic;
        reset_n         : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end PC_16bits_vlg_sample_tst;
