library verilog;
use verilog.vl_types.all;
entity adder_16bit_vlg_sample_tst is
    port(
        x_in            : in     vl_logic_vector(15 downto 0);
        y_in            : in     vl_logic_vector(15 downto 0);
        sampler_tx      : out    vl_logic
    );
end adder_16bit_vlg_sample_tst;
