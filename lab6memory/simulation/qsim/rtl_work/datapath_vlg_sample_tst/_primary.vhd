library verilog;
use verilog.vl_types.all;
entity datapath_vlg_sample_tst is
    port(
        ALU_func        : in     vl_logic;
        ALU_src         : in     vl_logic;
        clk             : in     vl_logic;
        clr_C           : in     vl_logic;
        clr_IR40        : in     vl_logic;
        ext_addr        : in     vl_logic_vector(15 downto 0);
        ext_data        : in     vl_logic_vector(15 downto 0);
        ext_we          : in     vl_logic;
        flag_ld         : in     vl_logic;
        IR_ld           : in     vl_logic;
        LH_sel          : in     vl_logic;
        maddr_sel       : in     vl_logic;
        mem_we          : in     vl_logic;
        OutR_ld         : in     vl_logic;
        PC_inc          : in     vl_logic;
        PC_ld           : in     vl_logic;
        PC_src          : in     vl_logic_vector(1 downto 0);
        Raa_sel         : in     vl_logic;
        Rd_sel          : in     vl_logic_vector(1 downto 0);
        reset_n         : in     vl_logic;
        RF_we           : in     vl_logic;
        sub_add         : in     vl_logic;
        test_normal     : in     vl_logic;
        WDR_ld          : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end datapath_vlg_sample_tst;
