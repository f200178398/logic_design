// mux_2to1_3bits
module mux_2to1_3bits(
    input[2:0] A,B,   
    input sel,
    output reg [2:0] Y
);

    always @(*)
        begin
            case(sel)
                1'b0: Y=A;
                1'b1: Y=B;
            endcase
        end

endmodule