// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module ALU_Adder_Subtractor_16bit_verilog_tb;
// internal signals declaration

    reg      [15:0] x_in;
    reg      [15:0] y_in;
    reg      mode,c_in;

    wire     [15:0] sum;
    wire     Z,V,N,C;

	//Unit Under Test instance and port map
	ALU_Adder_Subtractor_16bit_verilog UUT(.x_in(x_in),.y_in(y_in),.mode(mode),.c_in(c_in),
                                           .sum(sum),.Z(Z),.V(V),.N(N),.C(C));
    


//     module ALU_Adder_Subtractor_16bit_verilog(
//     input      [15:0] x_in,
//     input      [15:0] y_in,
//     input      mode,c_in,

//     output     [15:0] sum,
//     output     Z,V,N,C
   
// );

	
    initial begin
	 
            mode=1'b0;c_in=1'b0; 
      #180  mode=1'b1;c_in=1'b1;
				
    end
    initial begin
            x_in=0; y_in=0;
          #40 x_in=-32768; y_in=32766;
		  #40 x_in=32767; y_in=1;
		  #40 x_in=-1;
		  #40 y_in=0;
		  #50 x_in=32766; y_in=32767;
		  #40 x_in=-32768; y_in=-32767;
		  #40 x_in=-32768; y_in=32767;
		  #40 x_in=-1; y_in=0;

    end
	initial #1000 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h %h %h %h %h %h",x_in,y_in,mode,c_in,sum,Z,V,N,C);
endmodule
