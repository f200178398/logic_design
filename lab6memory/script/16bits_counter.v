// a mod 5 clk
module 16bits_counter(
    input [15:0]din,
    input clk,reset_n,load,en,
    output reg [15:0]qout
);

    always @(posedge clk or negedge reset_n)
        begin
            if(!reset_n) qout<=0; 
            else if(load) qout<=din;
            else if(en) qout<=qout+1;
            
        end
endmodule