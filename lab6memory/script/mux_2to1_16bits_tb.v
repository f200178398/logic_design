// a test bench for mux_2to1_16bits module
`timescale 1 ns/100 ps
module mux_2to1_16bits_tb;
// internal signals declaration

    reg [15:0] A,B;
    reg sel;
    wire [15:0] Y;
    //Unit Under Test instance and port map
    mux_2to1_16bits UUT(.A(A),.B(B),.sel(sel),.Y(Y));


    initial begin
       A=65530;
       B=65531;
    end
    initial begin
       #50  sel=1'b0;
       #50  sel=1'b1;  
       #50  sel=1'b0;
       #100 sel=1'b1;
	   #100 sel=1'b0;
    end
   
    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h %h",A,B,sel,Y);
endmodule

