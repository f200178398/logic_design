// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module RF_plus_ALU_verilog_tb;
// internal signals declaration

    reg      [15:0] ext_data;
    reg      [2:0] WR_addr,ext_addr;
    reg      clk,test_normal,ALU_func;
    reg      RF_WE,ext_WE;
    reg      [2:0] RA_addr,RB_addr;

    wire     [15:0] RA_data,RB_data;
    wire      V,N,C,Z;

	//Unit Under Test instance and port map
	RF_plus_ALU_verilog UUT(.ext_data(ext_data),
                            .WR_addr(WR_addr),.ext_addr(ext_addr),
                            .clk(clk),.test_normal(test_normal),.ALU_func(ALU_func),
                            .RF_WE(RF_WE),.ext_WE(ext_WE),
                            .RA_addr(RA_addr),.RB_addr(RB_addr),          

                            .RA_data(RA_data),.RB_data(RB_data), 
                            .V(V),.N(N),.C(C),.Z(Z));
    
// module RF_plus_ALU_verilog(
//     input      [15:0] ext_data,
//     input      [2:0] WR_addr,ext_addr,
//     input      clk,test_normal,ALU_func,
//     input      RF_WE,ext_WE,
//     input      [2:0] RA_addr,RB_addr,

//     output     [15:0] RA_data,RB_data,
//     output     V,N,C,Z
// );

  ///clk 
	always begin 
		    clk<=1'b0;
    #10;
        clk<=1'b1;
		#10;
	end 
  
  ///test_normal
  initial begin
         test_normal=1;
    #160 test_normal=0;
  end

integer i;
  ///ext_addr
  initial begin
    for(i=0;i<3;i=i+1)
    begin
              ext_addr=0;
        #20   ext_addr=1;
        #20   ext_addr=2;
        #20   ext_addr=3;
        #20   ext_addr=4;
        #20   ext_addr=5;
        #20   ext_addr=6;
        #20   ext_addr=7;
        #20;//沒寫會被ext_addr<=0;蓋掉 變成0
       // #20   ext_addr<=7;


    end
  end

  ///ext_data
  initial begin
    
    
              ext_data=16'h725E;
        #20   ext_data=16'h83EF;
        #20   ext_data=16'hEE6E;
        #20   ext_data=16'hFCD1;
        #20   ext_data=16'hF7E2;
        #20   ext_data=16'h38BD;
        #20   ext_data=16'hD598;
        #20   ext_data=16'hDF2F;
        #20   ext_data=16'h7915;
        #20   ext_data=16'hE6E2;
        #20   ext_data=16'h8C6F;
        #20;  ext_data=16'h0000;
       // #20   ext_addr<=7;
  end


    ///ext_WE
  initial begin
         ext_WE=1;
    #160 ext_WE=0;
  end

  ///ALU_func
  initial begin
         ALU_func=0;
    #340 ALU_func=1;
    #100 ALU_func=0;
  end
integer a;
   ///RA_addr
  initial begin
    
              RA_addr=3'b010;
        #20   RA_addr=3'b011;
        #20   RA_addr=3'b100;
        #20   RA_addr=3'b101;
        #20   RA_addr=3'b110;
        #20   RA_addr=3'b111;
		  #20;
        for(a=0;a<3;a=a+1)
          begin
                    RA_addr=0;
              #20   RA_addr=1;
              #20   RA_addr=2;
              #20   RA_addr=3;
              #20   RA_addr=4;
              #20   RA_addr=5;
              #20   RA_addr=6;
              #20   RA_addr=7;
              #20;//沒寫會被RA_addr=0;蓋掉 變成0
          end
       


  end

  ///RB_addr
  initial begin
    
              RB_addr=3'b001;
        #20   RB_addr=3'b010;      
        #20   RB_addr=3'b011;
        #20   RB_addr=3'b100;
        #20   RB_addr=3'b101;
        #20   RB_addr=3'b110;
        #20   RB_addr=3'b111;
		  #20;

                    RB_addr=0;
              #20   RB_addr=1;
              #20   RB_addr=2;
              #20   RB_addr=3;
              #20   RB_addr=4;
              #20   RB_addr=5;
              #20   RB_addr=6;
              #20   RB_addr=7;
              #20;//沒寫會被ext_addr<=0;蓋掉 變成0
            // #20   ext_addr<=7;

				        RB_addr=0;
              #20   RB_addr=1;
              #20   RB_addr=2;
              #20   RB_addr=2;
              #20   RB_addr=4;
              #20   RB_addr=5;
              #20   RB_addr=6;
              #20   RB_addr=7;
				  #20   RB_addr=0;
		        #20   RB_addr=1;

  end
  /// RF_WE
  initial begin
    
              RF_WE=0;
        #240  RF_WE=1;      
        #20   RF_WE=0;
        #40   RF_WE=1;
        #20   RF_WE=0;
        #40   RF_WE=1;
        #20   RF_WE=0;
        #40   RF_WE=1;
        #20   RF_WE=0;
  end
  ///WR_addr
  initial begin
    
              WR_addr=3'b010;
        #20   WR_addr=3'b110;    
        #60   WR_addr=3'b101; 
        #20   WR_addr=3'b001; 
        #40   WR_addr=3'b110;
        #20   WR_addr=3'b111;
        #20   WR_addr=3'b001;
        #20   WR_addr=3'b000;
        #20   WR_addr=3'b111;
        #20   WR_addr=3'b010;
        #20   WR_addr=3'b111;
        #20   WR_addr=3'b010; 
        #20   WR_addr=3'b111;
        #20   WR_addr=3'b101;
        #20   WR_addr=3'b100;
        #20   WR_addr=3'b011;
        #20   WR_addr=3'b000;
        #60   WR_addr=3'b011;
  end



	initial #1000 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h  %h %h %h %h  %h %h %h %h  %h %h %h %h",ext_data,WR_addr,ext_addr,clk,test_normal,ALU_func,RF_WE,ext_WE,RA_addr,RB_addr,RA_data,RB_data,V,N,C,Z);
endmodule
