// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module 16bits_counter_tb;
// internal signals declaration

    reg [15:0]din;
    reg clk,reset_n,load,en;
    wire [15:0]qout;
    //Unit Under Test instance and port map
    16bits_counter UUT(.din(din),
                       .clk(clk),.reset_n(reset_n),.load(load),.en(en),
                       .qout(qout)
                      );
    


    initial begin
            din=16'h2FFF;
    end

    
    initial begin
               reset_n=1'b0;
        #120   reset_n=1'b1;
    end
   
    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h",din,clk,reset_n,load,en,qout);
endmodule

