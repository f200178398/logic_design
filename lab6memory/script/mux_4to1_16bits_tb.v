// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module mux_4to1_16bits_tb;
// internal signals declaration

    reg [15:0] A0,A1,A2,A3;
    reg [1:0]sel;
    wire [15:0] Y;
    //Unit Under Test instance and port map
    mux_4to1_16bits UUT(.A0(A0),.A1(A1),.A2(A2),.A3(A3),.sel(sel),.Y(Y));


    initial begin
       A0=65530;
       A1=65531;
       A2=65532;
       A3=65533;
    end
    initial begin
            sel=2'b00;
       #50  sel=2'b01;  
       #50  sel=2'b10;
       #100 sel=2'b11;
	   #100 sel=2'b00;
    end
   
    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h %h %h %h",A0,A1,A2,A3,sel,Y);
endmodule

