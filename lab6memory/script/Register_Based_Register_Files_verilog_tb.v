// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module Register_Based_Register_Files_verilog_tb;
// internal signals declaration
    reg    [2:0] WR_addr;
    reg    [15:0] WR_data;
    reg    WE,clk;

    wire    [15:0] RA_data;
    reg     [2:0] RA_addr;
    wire    [15:0] RB_data;
    reg     [2:0] RB_addr;

	//Unit Under Test instance and port map
	Register_Based_Register_Files_verilog UUT(
                    .WR_addr(WR_addr),
                    .WR_data(WR_data),
                    .WE(WE),.clk(clk),
                    
                    .RA_data(RA_data),
                    .RA_addr(RA_addr),
                    .RB_data(RB_data),
                    .RB_addr(RB_addr));
//     module Register_Based_Register_Files_verilog(
//     input      [2:0] WR_addr,
//     input      [15:0] WR_data,
//     input      WE,clk,

//     output     [15:0] RA_data,
//     output     [2:0] RA_addr,
//     output     [15:0] RB_data,
//     output     [2:0] RB_addr
// );
    ///clk 
	always begin 
		
		clk<=1'b0;
      #20;
      clk<=1'b1;
		#20;
	end 
	

    initial begin
            WR_addr<=0;
      #70   WR_addr<=1;
		#40   WR_addr<=2;
		#40   WR_addr<=3;
      #50   WR_addr<=4;
      #30   WR_addr<=5;
      #40   WR_addr<=6;
		#40   WR_addr<=7;
		#40   WR_addr<=0;
    end

    initial begin
            WR_data<=16'h2B50;
      #70   WR_data<=16'h2B51;
		#40   WR_data<=16'h2B52;
		#40   WR_data<=16'h2B53;
      #50   WR_data<=16'h2B54;
      #30   WR_data<=16'h2B55;
      #40   WR_data<=16'h2B56;
		#40   WR_data<=16'h2B57;
		#40   WR_data<=16'h0034;
    end
	 
	 initial begin
            WE<=1'b0;
      #30   WE<=1'b1;
		#340  WE<=1'b0;
    end
	 
	 initial begin
            RA_addr<=0;
      #440  RA_addr<=1;
		#40   RA_addr<=2;
		#40   RA_addr<=3;
      #40   RA_addr<=4;
      #40   RA_addr<=5;
      #40   RA_addr<=6;
		#40   RA_addr<=7;
		#40   RA_addr<=0;
    end
	 
	 initial begin
            RB_addr<=0;
      #440  RB_addr<=1;
		#40   RB_addr<=2;
		#40   RB_addr<=3;
      #40   RB_addr<=4;
      #40   RB_addr<=5;
      #40   RB_addr<=6;
		#40   RB_addr<=7;
		#40   RB_addr<=0;
    end
	initial #1000 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h %h %h %h %h",clk,WR_addr,WR_data,WE, RA_data,RA_addr,RB_data,RB_addr);
endmodule
