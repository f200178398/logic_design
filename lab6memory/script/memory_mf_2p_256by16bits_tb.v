// a test bench for memory_mf_2p_256by16bits module
`timescale 1 ns/100 ps
module memory_mf_2p_256by16bits_tb;
// internal signals declaration
    reg     clk;
    reg     [15:0] data;
    reg     [7:0]  addr;
    reg     wren;
    wire    [15:0] qout;



//  module memory_mf_2p_256by16bits(
//     input      clock,
//     input     [15:0] data,
//     input     [7:0]  rdaddress,
//     input     [7:0]  wraddress,
//     input     wren,
//     output    [15:0] q
// );


	memory_mf_2p_256by16bits UUT(
                    .clock(clk),
                    .data(data),
                    .addr(addr),
                    .wren(wren),
                    .q(qout)
                    );

    ///clk 
	always begin 
		
	  clk<=1'b0;
     #20;
     clk<=1'b1;
	  #20;
	end 
	
    ///data 
    initial begin
          data<=16'hA000;
    #40   data<=16'hA001;
	  #40   data<=16'hA002;
	  #40   data<=16'hA003;
	  #40   data<=16'hAAAA;
	 
    end

    ///addr
    initial begin
          addr<=8'h10;
    #40   addr<=8'h11;
		#40   addr<=8'h12;
		#40   addr<=8'h13;
		#40   addr<=8'h10;
		#40   addr<=8'h11;
		#40   addr<=8'h12;
		#40   addr<=8'h13;

    end
	 
     ///wren
	 initial begin
            
             wren<=1'b1;
	   #130  wren<=1'b0;
    end
	 

	initial #1000 $finish;

	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h %h ",clk, data,addr,wren,qout);
    
endmodule