// sixteen_bits_register
module RF_plus_ALU_verilog(
    input      [15:0] ext_data,
    input      [2:0] WR_addr,ext_addr,
    input      clk,test_normal,ALU_func,
    input      RF_WE,ext_WE,
    input      [2:0] RA_addr,RB_addr,

    output     [15:0] RA_data,RB_data,
    output     V,N,C,Z
);

wire [15:0] sum,WR_data;
wire WE;
wire  [2:0] yout_to_WR_addr;
// module mux_2to1_16bits(
//     input [15:0] A,B,   
//     input sel,
//     output reg [15:0] Y
// );
mux_2to1_16bits mux16(
    .A(sum),.B(ext_data),   
       .sel(test_normal),
         .Y(WR_data)
);

// module mux_2to1_3bits(
//     input[2:0] A,B,   
//     input sel,
//     output reg [2:0] Y
// );

mux_2to1_3bits mux3(
      .A(WR_addr),.B(ext_addr),   
         .sel(test_normal),
           .Y(yout_to_WR_addr)
);

// module mux_2to1_1bits(
//     input A,B,   
//     input sel,
//     output reg  Y
// );

mux_2to1_1bits mux1(
    .A(RF_WE),.B(ext_WE),   
       .sel(test_normal),
         .Y(WE)
);


// module Register_Based_Register_Files_verilog(
//     input      [2:0] WR_addr,
//     input      [15:0] WR_data,
//     input      WE,clk,

//     output     [15:0] RA_data,
//     input      [2:0] RA_addr,
//     output     [15:0] RB_data,
//     input      [2:0] RB_addr
// );

Register_Based_Register_Files_verilog RF0(
                     .WR_addr(yout_to_WR_addr),
                     .WR_data(WR_data),
                   .WE(WE),.clk(clk),

                     .RA_addr(RA_addr),
                     .RA_data(RA_data),
                     .RB_addr(RB_addr),
                     .RB_data(RB_data)
);

// module ALU_Adder_Subtractor_16bit_verilog(
//     input      [15:0] x_in,
//     input      [15:0] y_in,
//     input      mode,c_in,

//     output     [15:0] sum,
//     output     Z,V,N,C
   
// );
ALU_Adder_Subtractor_16bit_verilog ALU0(
                     .x_in(RA_data),
                     .y_in(RB_data),
                     .mode(ALU_func),.c_in(ALU_func),

                     .sum(sum),
                     .V(V),.N(N),.C(C),.Z(Z)
   
);



endmodule