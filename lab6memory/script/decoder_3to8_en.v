// a mod 5 clk
module decoder_3to8_en(
    input [2:0]x_in,
    input en,
    output reg [7:0] Y
);

    always @(x_in or en)
        begin
            if(!en) Y=8'b0000_0000; 
            else 
                case(x_in)
                    3'b000: Y=8'b0000_0001;
                    3'b001: Y=8'b0000_0010;
                    3'b010: Y=8'b0000_0100;
                    3'b011: Y=8'b0000_1000;
                    
                    3'b100: Y=8'b0001_0000;
                    3'b101: Y=8'b0010_0000;
                    3'b110: Y=8'b0100_0000;
                    3'b111: Y=8'b1000_0000;
                endcase
        end
endmodule