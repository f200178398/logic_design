// sixteen_bits_register RF
module Register_Based_Register_Files_verilog(
    input      [2:0] WR_addr,
    input      [15:0] WR_data,
    input      WE,clk,

    output     [15:0] RA_data,
    input      [2:0] RA_addr,
    output     [15:0] RB_data,
    input      [2:0] RB_addr
);
wire [7:0] Yload;
wire [15:0] qout0,qout1,qout2,qout3 ,qout4,qout5,qout6,qout7;

/// 3 to 8 decoder 
// module decoder_3to8_en(
//     input [2:0]x_in,
//     input en,
//     output reg [7:0] Y
// );
decoder_3to8_en decode0(
    .x_in(WR_addr),
    .en(WE),
    .Y(Yload)
);

/// 8個 register 
// module sixteen_bits_register_with_load(
//     input [15:0] din,
//     input load,
//     input clk,
//     output reg [15:0] qout
// );
sixteen_bits_register_with_load R0(
    .din(WR_data),
    .load(Yload[0]),
    .clk(clk),
    .qout(qout0)
);
sixteen_bits_register_with_load R1(
    .din(WR_data),
    .load(Yload[1]),
    .clk(clk),
    .qout(qout1)
);
sixteen_bits_register_with_load R2(
    .din(WR_data),
    .load(Yload[2]),
    .clk(clk),
    .qout(qout2)
);
sixteen_bits_register_with_load R3(
    .din(WR_data),
    .load(Yload[3]),
    .clk(clk),
    .qout(qout3)
);

sixteen_bits_register_with_load R4(
    .din(WR_data),
    .load(Yload[4]),
    .clk(clk),
    .qout(qout4)
);
sixteen_bits_register_with_load R5(
    .din(WR_data),
    .load(Yload[5]),
    .clk(clk),
    .qout(qout5)
);
sixteen_bits_register_with_load R6(
    .din(WR_data),
    .load(Yload[6]),
    .clk(clk),
    .qout(qout6)
);
sixteen_bits_register_with_load R7(
    .din(WR_data),
    .load(Yload[7]),
    .clk(clk),
    .qout(qout7)
);


/// 8to1 Mux
//module mux_8to1_16bits( input  [15:0] A0, A1, A2, A3, A4, A5, A6, A7,
//                         input  [2:0] sel,
//                         output [15:0] Y);
mux_8to1_16bits muxA(.A0(qout0), .A1(qout1), .A2(qout2), .A3(qout3), .A4(qout4), .A5(qout5), .A6(qout6), .A7(qout7),
                     .sel(RA_addr),
                     .Y(RA_data));

mux_8to1_16bits muxB(.A0(qout0), .A1(qout1), .A2(qout2), .A3(qout3), .A4(qout4), .A5(qout5), .A6(qout6), .A7(qout7),
                     .sel(RB_addr),
                     .Y(RB_data));


endmodule