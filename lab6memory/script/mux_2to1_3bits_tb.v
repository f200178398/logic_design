// a test bench for mux_2to1_3bits module
`timescale 1 ns/100 ps
module mux_2to1_3bits_tb;
// internal signals declaration

    reg [2:0] A,B;
    reg sel;
    wire [2:0] Y;
    //Unit Under Test instance and port map
    mux_2to1_3bits UUT(.A(A),.B(B),.sel(sel),.Y(Y));


    initial begin
       A=5;
       B=6;
    end
    
    initial begin
            sel=1'b0;
      #50   sel=1'b1;  
      #50   sel=1'b0;
      #100  sel=1'b1;
	   #100  sel=1'b0;
    end
   
    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h %h",A,B,sel,Y);
endmodule

