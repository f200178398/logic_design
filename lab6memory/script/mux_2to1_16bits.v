// mux_2to1_16bits
module mux_2to1_16bits(
    input [15:0] A,B,   
    input sel,
    output reg [15:0] Y
);

    always @(*)
        begin
            case(sel)
                1'b0: Y=A;
                1'b1: Y=B;
            endcase
        end

endmodule