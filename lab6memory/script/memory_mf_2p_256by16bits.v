// memory_mf_2p_256by16bits
module memory_mf_2p_256by16bits(
    input      clock,
    input     [15:0] data,
    input     [7:0]  addr,
    input     wren,
    output    [15:0] q
);

/// megafunction generate ram module
// module memory_256WORDS_16bits (
// 	clock,
// 	data,
// 	rdaddress,
// 	wraddress,
// 	wren,
// 	q);

/// megafunction generate ram module
memory_256WORDS_16bits memory256 (
	.clock(clock),
	.data(data),
	.rdaddress(addr),
	.wraddress(addr),
	.wren(wren),
	.q(q));

endmodule