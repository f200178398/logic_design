// sixteen_bits_register
module memory_module_verilog(
    input      [15:0] ALU_out,
    input      [15:0] PC_out,
    input             maddr_sel,

    input      [15:0] ext_addr,
    input      [15:0] WDR_out,
    input      [15:0] ext_data,
   

    input             clk,
    input             mem_we,ext_we,test_normal,
    output     [15:0] mem_out
);

wire [15:0] addrA,y_out,Y_data;
wire Y_WE;


// module mux_2to1_16bits(
//     input [15:0] A,B,   
//     input sel,
//     output reg [15:0] Y
// );
mux_2to1_16bits mux1(
    .A(ALU_out),.B(PC_out),   
       .sel(maddr_sel),
         .Y(addrA)
);

mux_2to1_16bits mux2(
    .A(addrA),.B(ext_addr),   
       .sel(test_normal),
         .Y(y_out)
);

mux_2to1_16bits mux3(
    .A(WDR_out),.B(ext_data),   
       .sel(test_normal),
         .Y(Y_data)
);

// module mux_2to1_1bits(
//     input A,B,   
//     input sel,
//     output reg  Y
// );

mux_2to1_1bits mux22(
    .A(mem_we),.B(ext_we),   
    .sel(test_normal),
    .Y(Y_WE) 
);


// // memory_mf_2p_256by16bits
// module memory_mf_2p_256by16bits(
//     input      clock,
//     input     [15:0] data,
//     input     [7:0]  addr,
//     input     wren,
//     output    [15:0] q
// );
memory_mf_2p_256by16bits memory(
    .clock(clk),
    .data(Y_data),
    .addr(y_out[7:0]),
    .wren(Y_WE),
    .q(mem_out)
);

endmodule