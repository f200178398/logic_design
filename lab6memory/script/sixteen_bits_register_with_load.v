// sixteen_bits_register
module sixteen_bits_register_with_load(
    input clk,load,
    input      [15:0] din,
    output reg [15:0] qout
);



always @(posedge clk)
    begin
        if(load) 
            qout<=din;

        else
            qout<=qout;

    end
endmodule