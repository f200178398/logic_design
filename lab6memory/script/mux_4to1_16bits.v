// mux_4to1_16bits
module mux_4to1_16bits(
    input [15:0] A0,A1,A2,A3,
    input [1:0]sel,
    output reg [15:0] Y
);

    always @(*)
        begin
            case(sel)
                2'b00: Y=A0;
                2'b01: Y=A1;
                2'b10: Y=A2;
                2'b11: Y=A3;
            endcase
        end
endmodule