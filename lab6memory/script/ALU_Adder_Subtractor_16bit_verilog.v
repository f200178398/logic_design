// sixteen_bits_register
module ALU_Adder_Subtractor_16bit_verilog(
    input      [15:0] x_in,
    input      [15:0] y_in,
    input      mode,c_in,

    output     [15:0] sum,
    output     Z,V,N,C
   
);

wire [15:0] t;
wire [15:1] c;


///xor operation
genvar i;
generate
    for(i=0;i<=15;i=i+1)
    begin:xor_use 
        xor bit0(t[i],y_in[i],mode);
    end
endgenerate


// module full_adder_behavioral(
// 		input a,b,Cin,
// 		output reg Cout,Sum);

///full adder
generate
        full_adder_behavioral adder_start(
                    .a(x_in[0]),.b(t[0]),.Cin(c_in),
                    .Cout(c[1]),.Sum(sum[0]));
	 ///use loop					  
    for(i=1;i<=14;i=i+1)
    begin:full_adder_use 
        full_adder_behavioral adder1(
                    .a(x_in[i]),.b(t[i]),.Cin(c[i]),
                    .Cout(c[i+1]),.Sum(sum[i]));
    end
	 ///
    full_adder_behavioral adder_end(
                    .a(x_in[15]),.b(t[15]),.Cin(c[15]),
                    .Cout(C),.Sum(sum[15]));

endgenerate


/// compute flags N,Z,V 
assign N=sum[15];
assign Z=~(|sum);  //not (sum[0] or sum[1] ...or sum[15]) 暹銝0
assign V=c[15]^C; //c[15] xor C know if is overflow or not

endmodule