// a test bench for mux_8to1_16bits module
`timescale 1 ns/100 ps
module mux_8to1_16bits_tb;
// internal signals declaration

    reg [15:0] A0, A1, A2, A3, A4, A5, A6, A7;
    reg [2:0] sel;
    wire [15:0] Y;
    //Unit Under Test instance and port map
    mux_8to1_16bits UUT(.A0(A0),.A1(A1),.A2(A2),.A3(A3),.A4(A4),.A5(A5),.A6(A6),.A7(A7),.sel(sel),.Y(Y));


    initial begin
       A0=55530;
       A1=55531;
       A2=55532;
       A3=55533;
       A4=55534;
       A5=55535;
       A6=55536;
       A7=55537;
    end
    initial begin
           sel=3'b000;
      #50  sel=3'b001;  
      #50  sel=3'b010;
      #100 sel=3'b011;
      #50  sel=3'b100;  
      #50  sel=3'b101;
      #100 sel=3'b110;
      #50  sel=3'b111;  
	  #100  sel=3'b000;

      
    end
   
    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h %h %h %h %h %h %h %h",A0,A1,A2,A3,A4,A5,A6,A7,sel,Y);
endmodule

