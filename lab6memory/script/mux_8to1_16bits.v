// a mux_8to1_16bits
module mux_8to1_16bits(
    input  [15:0] A0, A1, A2, A3, A4, A5, A6, A7,
    input  [2:0] sel,
    output [15:0] Y
);
wire [15:0] YA, YB;
wire [1:0] subsel;
assign subsel={sel[1], sel[0]};
//module mux_4to1_16bits(input [15:0] A0,A1,A2,A3,input [1:0]sel,output reg [15:0] Y);
mux_4to1_16bits mux_4to1_A( .A0(A0), .A1(A1), .A2(A2), .A3(A3), .sel(subsel), .Y(YA));
mux_4to1_16bits mux_4to1_B( .A0(A4), .A1(A5), .A2(A6), .A3(A7), .sel(subsel), .Y(YB));
//module mux_2to1_16bits(input [15:0] A,B,input sel,output reg [15:0] Y);
mux_2to1_16bits mux_2to1( .A(YA), .B(YB), .sel(sel[2]), .Y(Y));

endmodule