// sixteen_bits_register
module sixteen_bits_register_with_shift
#(parameter N=2)
(
    input clk,shift,
    input [15:0] din,
    input [2:0] shift_number,
    output reg [15:0] qout
);



always @(posedge clk)
    begin
        if(shift) 
            qout<=(din<<shift_number);

        else
            qout<=qout;

    end
endmodule