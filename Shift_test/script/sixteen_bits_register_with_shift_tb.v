// a test bench for the mod_up_clk module
`timescale 1 ns/100 ps
module sixteen_bits_register_with_shift_tb;
// internal signals declaration


    reg clk,shift;
    reg [15:0] din;
    reg [2:0] shift_number;
    wire [15:0] qout;
    //Unit Under Test instance and port map
    sixteen_bits_register_with_shift UUT(.clk(clk),.shift(shift),.din(din),.shift_number(shift_number),.qout(qout));

    always begin //stimulus generation block
         
        clk<=1'b0;
        #10;
        clk<=1'b1;
        #10;
    end 


    initial begin
             shift<=1'b0;
        #50  shift<=1'b1;
        #100 shift<=1'b0;
		  #50  shift<=1'b1;
        #100 shift<=1'b0;
    end
    initial begin
             din<=2;
        #50  din<=20;
      
    end
     initial begin
             shift_number<=2;
        #50  shift_number<=1;
		  #50  shift_number<=3;
      
    end

    initial #1000 $finish;
    initial  //response monitoring block
        $monitor($realtime,"ns %h %h %h %h %h",clk,shift,din,shift_number,qout);
endmodule

