library verilog;
use verilog.vl_types.all;
entity sixteen_bits_register_with_shift is
    generic(
        N               : integer := 2
    );
    port(
        clk             : in     vl_logic;
        shift           : in     vl_logic;
        din             : in     vl_logic_vector(15 downto 0);
        shift_number    : in     vl_logic_vector(2 downto 0);
        qout            : out    vl_logic_vector(15 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of N : constant is 1;
end sixteen_bits_register_with_shift;
