transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+D:/DigitalDesignlabs/Shift_test/script {D:/DigitalDesignlabs/Shift_test/script/sixteen_bits_register_with_shift.v}

vlog -vlog01compat -work work +incdir+D:/DigitalDesignlabs/Shift_test/script {D:/DigitalDesignlabs/Shift_test/script/sixteen_bits_register_with_shift_tb.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneive_ver -L rtl_work -L work -voptargs="+acc"  sixteen_bits_register_with_shift_tb

add wave *
view structure
view signals
run -all
