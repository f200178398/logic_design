// Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, the Altera Quartus Prime License Agreement,
// the Altera MegaCore Function License Agreement, or other 
// applicable license agreement, including, without limitation, 
// that your use is for the sole purpose of programming logic 
// devices manufactured by Altera and sold by Altera or its 
// authorized distributors.  Please refer to the applicable 
// agreement for further details.

// PROGRAM		"Quartus Prime"
// VERSION		"Version 15.1.0 Build 185 10/21/2015 SJ Lite Edition"
// CREATED		"Thu Sep 10 14:50:45 2020"

module full_adder_1bit(
	a,
	b,
	Cin,
	Sum,
	Cout
);


input wire	a;
input wire	b;
input wire	Cin;
output wire	Sum;
output wire	Cout;

wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;




assign	SYNTHESIZED_WIRE_2 = b & a;

assign	SYNTHESIZED_WIRE_3 = SYNTHESIZED_WIRE_4 & Cin;

assign	SYNTHESIZED_WIRE_4 = a ^ b;

assign	Sum = Cin ^ SYNTHESIZED_WIRE_4;

assign	Cout = SYNTHESIZED_WIRE_2 | SYNTHESIZED_WIRE_3;


endmodule
