library verilog;
use verilog.vl_types.all;
entity full_adder_1bit is
    port(
        a               : in     vl_logic;
        b               : in     vl_logic;
        Cin             : in     vl_logic;
        Sum             : out    vl_logic;
        Cout            : out    vl_logic
    );
end full_adder_1bit;
