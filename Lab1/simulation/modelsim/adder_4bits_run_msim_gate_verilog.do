transcript on
if {[file exists gate_work]} {
	vdel -lib gate_work -all
}
vlib gate_work
vmap work gate_work

vlog -vlog01compat -work work +incdir+. {adder_4bits.vo}

vlog -vlog01compat -work work +incdir+D:/DigitalDesignlabs/Lab1/src {D:/DigitalDesignlabs/Lab1/src/full_adder_1bit_tb.v}

vsim -t 1ps +transport_int_delays +transport_path_delays -L maxv_ver -L gate_work -L work -voptargs="+acc"  full_adder_1bit_tb

add wave *
view structure
view signals
run -all
