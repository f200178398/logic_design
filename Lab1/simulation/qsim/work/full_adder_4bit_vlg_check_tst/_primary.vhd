library verilog;
use verilog.vl_types.all;
entity full_adder_4bit_vlg_check_tst is
    port(
        C_out           : in     vl_logic;
        sum             : in     vl_logic_vector(3 downto 0);
        sampler_rx      : in     vl_logic
    );
end full_adder_4bit_vlg_check_tst;
