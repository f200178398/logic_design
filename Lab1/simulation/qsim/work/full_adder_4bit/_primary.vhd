library verilog;
use verilog.vl_types.all;
entity full_adder_4bit is
    port(
        C_out           : out    vl_logic;
        c_in            : in     vl_logic;
        x_in            : in     vl_logic_vector(3 downto 0);
        y_in            : in     vl_logic_vector(3 downto 0);
        sum             : out    vl_logic_vector(3 downto 0)
    );
end full_adder_4bit;
