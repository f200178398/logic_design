//a full adder
module full_adder_behavioral(
		input a,b,Cin,
		output reg Cout,Sum);
	
	//the body of the full adder
	always @(a,b,Cin)
	begin 
		 {Cout,Sum}=a+b+Cin;
	end
	//assign{Cout,Sum}=a+b+Cin;
endmodule

