// a test bench for the full adder in structural style
`timescale 1 ns/100 ps
module full_adder_structural_tb;
// internal signals declaration

	reg a,b,Cin;
	wire Sum,Cout;

	//Unit Under Test instance and port map
	full_adder_structural UUT(.a(a),.b(b),.Cin(Cin),.Sum(Sum),.Cout(Cout));

	initial begin //stimulus generation block
		a= 1'b0; b= 1'b0; Cin= 1'b0;
		#10;
		a= 1'b0; b= 1'b0; Cin= 1'b1;
		#10;
		a= 1'b0; b= 1'b1; Cin= 1'b0;
		#10;
		a= 1'b0; b= 1'b1; Cin= 1'b1;
		#10;
		a= 1'b1; b= 1'b0; Cin= 1'b0;
		#10;
		a= 1'b1; b= 1'b0; Cin= 1'b1;
		#10;
		a= 1'b1; b= 1'b1; Cin= 1'b0;
		#10;
		a= 1'b1; b= 1'b1; Cin= 1'b1;
		#10;
	end 
	initial #250 $finish;
	initial  //response monitoring block
		$monitor($realtime,"ns %h %h %h %h",a,b,Cin,{Cout,Sum});
endmodule
