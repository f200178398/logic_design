//a full adder
module full_adder_hierarchical(
		input a,b,Cin,
		output Cout,Sum);
	wire S1,C1,C2; //outputs of both half adders
	//the body of the full adder
	half_adder ha_1(a,b,C1,S1);
	half_adder ha_2(Cin,S1,C2,Sum);
	or(Cout,C1,C2);
endmodule

module half_adder(input x,y, output c,s);
	xor(s,x,y);
	and(c,x,y);
endmodule