//a full adder
module full_adder_structural(
		input a,b,Cin,
		output Cout,Sum);
	wire S1,C1,C2;
	//the body of the full adder

	and(C1,a,b);
	xor(S1,a,b);

	and(C2,S1,Cin);
	xor(Sum,Cin,S1);

	or(Cout,C2,C1);


endmodule