//a full adder
module full_adder_dataflow(
		input a,b,Cin,
		output Cout,Sum);
	
	//the body of the full adder
	assign{Cout,Sum}=a+b+Cin;
endmodule

