transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+D:/DigitalDesignlabs/Lab2_Verlog/script {D:/DigitalDesignlabs/Lab2_Verlog/script/full_adder_behavioral.v}

vlog -vlog01compat -work work +incdir+D:/DigitalDesignlabs/Lab2_Verlog/script {D:/DigitalDesignlabs/Lab2_Verlog/script/full_adder_behavioral_tb.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L arriaii_hssi_ver -L arriaii_pcie_hip_ver -L arriaii_ver -L rtl_work -L work -voptargs="+acc"  full_adder_behavioral_tb

add wave *
view structure
view signals
run -all
